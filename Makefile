prefix = $(HOME)
libdir = $(prefix)/lib
INSTALL = install
CC = gcc
CFLAGS = -O2 -Wall -g

all: libnodelay.so
libnodelay.so: nodelay.c
	$(CC) $(CFLAGS) -fPIC -Wall -W -o $@ -shared $< -ldl
clean:
	$(RM) $(shell cat .gitignore)
install: libnodelay.so
	$(INSTALL) -m 755 -d $(DESTDIR)$(libdir)
	$(INSTALL) -m 644 $< $(DESTDIR)$(libdir)

test: libnodelay.so
	! ldd -r $^ | grep -q undefined
	test -n "$(TEST_PORT)" || { echo TEST_PORT not set; exit 1; }
	if [ -e test-server.pid ]; then echo test-server.pid exists; exit 1; fi
	if [ -e test-client.pid ]; then echo test-client.pid exists; exit 1; fi
	LD_PRELOAD=$(CURDIR)/$< \
		strace -o test-server.out \
				nc -l $(TEST_PORT) & \
		echo $$! > test-server.pid
	LD_PRELOAD=$(CURDIR)/$< \
		strace -o test-client.out \
				nc -q 1 127.0.0.1 $(TEST_PORT) & \
		echo $$! > test-client.pid
	sleep 1
	kill -9 `cat test-client.pid` || sleep 1
	kill -9 `cat test-server.pid` || sleep 1
	$(RM) test-server.pid test-client.pid
	grep setsockopt.*TCP_NODELAY test-server.out
	grep setsockopt.*TCP_NODELAY test-client.out
	$(RM) test-server.out test-client.out

index.html: title = $(shell sed -ne 1p < $<)
index.html: README
	markdown $< | (tidy -q 2>/dev/null || true) | \
	  sed "s,<title></title>,<title>$(title)</title>," > $@+
	mv $@+ $@
